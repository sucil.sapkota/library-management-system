# Library Management System
*It is a simple library management system where admin can manage books along with auther and students can apply for the book*

# Tech Used
- Python flask
- Mysql

# For migration
### To generate migration files
```
flask db init
flask db migrate -m "Initial migration"
```

### Migrate to database
```
flask db upgrade
```

### Seed to database
```
flask seed run
```

# Flask environment variables
| Name | Description |
| ----------- | ----------- |
| FLASK_APP | Name of the application i.e app |
| FLASK_ENV | Run time environment |
| FLASK_RUN_PORT | Port of the application |
| FLASK_RUN_HOST | Host of the application |
| FLASK_DEBUG | Boolean value to enable/disable debug mode |

# Application environment variables
| Name | Description |
| ----------- | ----------- |
| SECRET_KEY | Secret key of the application |
| DATABASE_URI | Database uri for connection |

# Additional links
- https://www.cyberciti.biz/faq/how-to-install-docker-on-amazon-linux-2/